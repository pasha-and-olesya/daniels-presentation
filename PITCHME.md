# Daniel Presents

A new gymnastics club

 ---

### My new gymnastics club

- Fun.
- Good exercise.
- Learn new things.

---

### What you need

- A water bottle.
- Your PE kit.

---

### Where and When

- Meet in the hall.
- Fridays at 4pm.

---

### What to expect

- We will use PE equipment:
  - benches
  - mats

---

### Come and see me for an application form!
